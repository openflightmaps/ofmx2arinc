#!/bin/sh
mkdir -p examples/
gsutil cp -r gs://snapshots-dev.openflightmaps.org/live/2004/ofmx/lovv/latest/isolated/* examples/
gsutil cp -r gs://snapshots-dev.openflightmaps.org/live/2004/ofmx/lsas/latest/isolated/* examples/
gsutil cp -r gs://snapshots-dev.openflightmaps.org/live/2004/ofmx/ed/latest/isolated/* examples/
