package arinc

type RestrictiveAirspace struct {
	RecordType           string `fixed:"1,1"`   //(1)1
	CustomerAreaCode     string `fixed:"2,4"`   //(3)2 thru 4
	SectionCode          string `fixed:"5,5"`   //(1)5
	SubsectionCode       string `fixed:"6,6"`   //(1)6
	ICAOCode             string `fixed:"7,8"`   //(2)7 thru 8
	RestrictiveType      string `fixed:"9,9"`   //(1)9
	Designation          string `fixed:"10,19"` //(10)10
	MultipleCode         string `fixed:"20,20"` //(1)20
	SequenceNumber       string `fixed:"21,24"` //(4)21
	ContinuationRecordNo string `fixed:"25,25"` //(1)25
	Level                string `fixed:"26,26"` //(1)26

	BoundaryVia string `fixed:"31,32"` //(2)31 thru 32

	Latitude  string `fixed:"33,41"` //(9)33 thru 41
	Longitude string `fixed:"42,51"` //(10)42 thru 51

	LowerLimit              string `fixed:"82,86"`   //(5)82 thru 86
	LowerUnitIndicator      string `fixed:"87,87"`   //(1)87
	UpperLimit              string `fixed:"88,92"`   //(5)88 thru 92
	UpperUnitIndicator      string `fixed:"93,93"`   //(1)93
	RestrictiveAirspaceName string `fixed:"94,123"`  //(30)94 thru 123
	FileRecordNo            string `fixed:"124,128"` //(5)124 thru 128
	CycleDate               string `fixed:"129,132"` //(4)129 thru 132
}

type ControlledAirspace struct {
	RecordType       string `fixed:"1,1"`   //(1)1
	CustomerAreaCode string `fixed:"2,4"`   //(3)2 thru 4
	SectionCode      string `fixed:"5,5"`   //(1)5
	SubsectionCode   string `fixed:"6,6"`   //(1)6
	ICAOCode         string `fixed:"7,8"`   //(2)7 thru 8
	AirspaceType     string `fixed:"9,9"`   //(1)9
	AirspaceCenter   string `fixed:"10,14"` //(5)10 thru 14

	AirspaceClassification string `fixed:"17,17"` //(1)17

	SequenceNumber       string `fixed:"21,24"` //(4)21
	ContinuationRecordNo string `fixed:"25,25"` //(1)25
	Level                string `fixed:"26,26"` //(1)26

	BoundaryVia string `fixed:"31,32"` //(2)31 thru 32

	Latitude  string `fixed:"33,41"` //(9)33 thru 41
	Longitude string `fixed:"42,51"` //(10)42 thru 51

	LowerLimit              string `fixed:"82,86"`   //(5)82 thru 86
	LowerUnitIndicator      string `fixed:"87,87"`   //(1)87
	UpperLimit              string `fixed:"88,92"`   //(5)88 thru 92
	UpperUnitIndicator      string `fixed:"93,93"`   //(1)93
	RestrictiveAirspaceName string `fixed:"94,123"`  //(30)94 thru 123
	FileRecordNo            string `fixed:"124,128"` //(5)124 thru 128
	CycleDate               string `fixed:"129,132"` //(4)129 thru 132
}
