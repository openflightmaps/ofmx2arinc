package arinc

// record type codes: ref specs section 5.2
const (
	RecordTypeStandard = "S"
	RecordTypeTailored = "T"
)

// Figure 5-1 Section and Subsection Encoding Scheme
const (
	SectionCodeMORA        = "A"
	SubSectionCodeGridMORA = "S"

	SectionCodeNavaid             = "D"
	SubSectionCodeVHFNavaid       = ""
	SubSectionCodeNDBNavaid       = "B"
	SubSectionCodeTACANDuplicates = "T"

	SectionCodeEnroute                 = "E"
	SubSectionCodeWaypoints            = "A"
	SubSectionCodeAirwayMarkers        = "M"
	SubSectionCodeHoldingPatterns      = "P"
	SubSectionCodeAirwaysAndRoutes     = "R"
	SubSectionCodeSpecialActivityAreas = "S"
	SubSectionCodePreferredRoutes      = "T"
	SubSectionCodeAirwayRestrictions   = "U"
	SubSectionCodeCommunications       = "V"

	SectionCodeHeliport              = "H"
	SubSectionCodeReferencePoints    = "A"
	SubSectionCodeTerminalWaypoints  = "C"
	SubSectionCodeSIDs               = "D"
	SubSectionCodeSTARs              = "E"
	SubSectionCodeApproachProcedures = "F"
	SubSectionCodeHelipads           = "H"
	SubSectionCodeTAA                = "K"
	SubSectionCodeMSA                = "S"
	SubSectionCodeSBASPathPoint      = "P"
	//SubSectionCodeCommunications = "V" // same as under other section

	SectionCodeAirport = "P"
	//SubSectionCodeReferencePoints = "A" // same as under other section
	SubSectionCodeGates = "B"
	//SubSectionCodeTerminalWaypoints  = "C" // same as under other section
	//SubSectionCodeSIDs = "D" // same as under other section
	//SubSectionCodeSTARs = "E" // same as under other section
	//SubSectionCodeApproachProcedures = "F" // same as under other section
	SubSectionCodeRunways = "G"
	//SubSectionCodeHelipads = "H" // same as under other section
	SubSectionCodeLocalizerGlideslope = "I"
	//SubSectionCodeTAA = "K" // same as under other section
	SubSectionCodeMLS             = "L"
	SubSectionCodeLocalizerMarker = "M"
	SubSectionCodeTerminalNDB     = "N"
	//SubSectionCodeSBASPathPoint = "P" // same as under other section
	SubSectionCodeGBASPathPoint     = "Q"
	SubSectionCodeFltPlanningArrDep = "R"
	//SubSectionCodeMSA = "S" // same as under other section
	SubSectionCodeGLSStation = "T"
	//SubSectionCodeCommunications = "V" // same as under other section

	SectionCodeCompanyRoutes                = "R"
	SubSectionCodeCompanyRoutes             = "" // (Master Airline File)
	SubSectionCodeAlternateRecords          = "A"
	SubSectionCodeHelicopterOperationRoutes = "H" // (Master Helicopter File)

	SectionCodeTables                   = "T"
	SubSectionCodeCruisingTables        = "C"
	SubSectionCodeGeographicalReference = "G"
	SubSectionCodeRNAVNameTable         = "N"
	SubSectionCodeCommunicationType     = "V"

	SectionCodeAirspace               = "U"
	SubSectionCodeControlledAirspace  = "C"
	SubSectionCodeFIR_UIR             = "F"
	SubSectionCodeRestrictiveAirspace = "R"
)

// Max Lengths
const (
	MaxLenWaypointIdent = 5
	MaxLenAirspaceName  = 30
	MaxLenDefault       = 30
)

// TODO, refactor
const (
	ARINC_TODO    = "TODO"
	ARINC_WGE     = "WGE"
	ARINC_ENROUTE = "ENRT"

	ARINC_MERIT_LOW = "1"

	ARINC_MAGNETIC = "M"
	ARINC_TRUE     = "T"

	ARINC_PUBLIC   = "P"
	ARINC_MILITARY = "M"

	ARINC_AREA_CODE_EUR = "EUR"
	ARINC_AREA_CODE_USA = "USA"
	ARINC_AREA_CODE_CAN = "CAN"

	ARINC_FREQ_FACTOR_VHF = 1000
	ARINC_FREQ_FACTOR_VOR = 100
	ARINC_FREQ_FACTOR_NDB = 10

	ARINC_SUBSECTION_CODE_VHF_NAVAID = " "
	ARINC_SUBSECTION_CODE_NDB_NAVAID = "B"

	ARINC_FREQ_WIDTH_VHF = 7
	ARINC_FREQ_UNIT_VHF  = "V"
	ARINC_FREQ_UNIT_833  = "C"

	ARINC_BOUNDARY_VIA_GREAT_CIRCLE     = "G "
	ARINC_BOUNDARY_VIA_GREAT_CIRCLE_END = "GE"

	ARINC_FIR_UIR_INDICATOR_FIR = "F"
	ARINC_FIR_UIR_INDICATOR_UIR = "U"

	ARINC_WAYPOINT_TYPE_VFR = "V"
)
