package arinc

type Waypoint struct {
	RecordType               string `fixed:"1,1"`     //(1)1
	CustomerAreaCode         string `fixed:"2,4"`     //(3)2 thru 4
	SectionCode              string `fixed:"5,5"`     //(1)5
	SubsectionCode           string `fixed:"6,6"`     //(1)6
	RegionCode               string `fixed:"7,10"`    //(4)7 thru 10
	ICAOCode1                string `fixed:"11,12"`   //(2)11 thru 12
	Subsection               string `fixed:"13,13"`   //(1)13
	WaypointIdentifier       string `fixed:"14,18"`   //(5)14 thru 18
	Blank19                  string `fixed:"19,19"`   //(1)19
	ICAOCode2                string `fixed:"20,21"`   //(2)20 thru 21
	ContinuationRecordNo     string `fixed:"22,22"`   //(1)22
	Blank23                  string `fixed:"23,26"`   //(4)23
	WaypointType             string `fixed:"27,29"`   //(3)27 thru 29
	WaypointUsage            string `fixed:"31,31"`   //(1)31
	Blank32                  string `fixed:"32,32"`   //(1)32
	WaypointLatitude         string `fixed:"33,41"`   //(9)33 thru 41
	WaypointLongitude        string `fixed:"42,51"`   //(10)42 thru 51
	DynamicMagneticVariation string `fixed:"75,79"`   //(5)75 thru 79
	DatumCode                string `fixed:"85,87"`   //(3)85 thru 87
	Reserved                 string `fixed:"88,95"`   //(8)88
	NameFormatIndicator      string `fixed:"96,98"`   //(3)96 thru 98
	WaypointNameDescription  string `fixed:"99,123"`  //(25)99 thru 123
	FileRecordNo             string `fixed:"124,128"` //(5)124 thru 128
	CycleDate                string `fixed:"129,132"` //(4)129 thru 132
}

type WaypointContinuationRecord struct {
	RecordType           string `fixed:"1,1"`     //(1)1
	CustomerAreaCode     string `fixed:"2,4"`     //(3)2 thru 4
	SectionCode          string `fixed:"5,5"`     //(1)5
	SubsectionCode       string `fixed:"6,6"`     //(1)6
	RegionCode           string `fixed:"7,10"`    //(4)7 thru 10
	ICAOCode1            string `fixed:"11,12"`   //(2)11 thru 12
	Subsection           string `fixed:"13,13"`   //(1)13
	WaypointIdentifier   string `fixed:"14,18"`   //(5)14 thru 18
	Blank19              string `fixed:"19,19"`   //(1)19
	ICAOCode2            string `fixed:"20,21"`   //(2)20 thru 21
	ContinuationRecordNo string `fixed:"22,22"`   //(1)22
	ApplicationType      string `fixed:"23,23"`   //(1)23
	Notes                string `fixed:"24,92"`   //(69)24 thru 92
	Reserved             string `fixed:"93,123"`  //(31)93
	FileRecordNo         string `fixed:"124,128"` //(5)124 thru 128
	CycleDate            string `fixed:"129,132"` //(4)129 thru 132
}
