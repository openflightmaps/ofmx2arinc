package arinc

type FirUir struct {
	RecordType           string `fixed:"1,1"`     //(1)1
	CustomerAreaCode     string `fixed:"2,4"`     //(3)2 thru 4
	SectionCode          string `fixed:"5,5"`     //(1)5
	SubsectionCode       string `fixed:"6,6"`     //(1)6
	FirUirIdentifier     string `fixed:"7,10"`    //(4)7 thru 10
	FirUirAddress        string `fixed:"11,14"`   //(4)11 thru 14
	FirUirIndicator      string `fixed:"15,15"`   //(1)15
	SequenceNumber       string `fixed:"16,19"`   //(4)16 thru 19
	ContinuationRecordNo string `fixed:"20,20"`   //(1)20
	BoundaryVia          string `fixed:"33,34"`   //(2)33 thru 34
	Latitude             string `fixed:"35,43"`   //(9)35 thru 43
	Longitude            string `fixed:"44,53"`   //(10)44 thru 53
	FirUpperLimit        string `fixed:"81,85"`   //(5)81 thru 85
	UirLowerLimit        string `fixed:"86,90"`   //(5)86 thru 90
	UirUpperLimit        string `fixed:"91,95"`   //(5)91 thru 95
	CruiseTableInd       string `fixed:"96,97"`   //(2)96 thru 97
	FirUirName           string `fixed:"99,123"`  //(25)99 thru 123
	FileRecordNo         string `fixed:"124,128"` //(5)124 thru 128
	CycleDate            string `fixed:"129,132"` //(4)129 thru 132
}
