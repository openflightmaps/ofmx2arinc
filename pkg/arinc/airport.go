package arinc

type Airport struct {
	RecordType              string `fixed:"1,1"`     //(1)1
	CustomerAreaCode        string `fixed:"2,4"`     //(3)2 thru 4
	SectionCode             string `fixed:"5,5"`     //(1)5
	Blank6                  string `fixed:"6,6"`     //(1)6
	AirportICAOIdentifier   string `fixed:"7,10"`    //(4)7 thru 10
	ICAOCode1               string `fixed:"11,12"`   //(2)11 thru 12
	SubsectionCode          string `fixed:"13,13"`   //(1)13
	IATADesignator          string `fixed:"14,16"`   //(3)14 thru 16
	Reserved                string `fixed:"17,18"`   //(2)17 thru 18
	Blank19                 string `fixed:"19,21"`   //(3)19 thru 21
	ContinuationRecordNo    string `fixed:"22,22"`   //(1)22
	SpeedLimitAltitude      string `fixed:"23,27"`   //(5)23 thru 27
	LongestRunway           string `fixed:"28,30"`   //(3)28 thru 30
	IFRCapability           string `fixed:"31,31"`   //(1)31
	LongestRwySurfaceCode   string `fixed:"32,32"`   //(1)32
	ARPLatitude             string `fixed:"33,41"`   //(9)33 thru 41
	ARPLongitude            string `fixed:"42,51"`   //(10)42 thru 51
	MagneticVariation       string `fixed:"52,56"`   //(5)52 thru 56
	AirportElevation        string `fixed:"57,61"`   //(5)57 thru 61
	SpeedLimit              string `fixed:"62,64"`   //(3)62 thru 64
	RecommendedNavAid       string `fixed:"65,68"`   //(4)65 thru 68
	ICAOCode2               string `fixed:"69,70"`   //(2)69 thru 70
	TransitionAltitude      string `fixed:"71,75"`   //(5)71 thru 75
	TransitionLevel         string `fixed:"76,80"`   //(5)76 thru 80
	PublicMilitaryIndicator string `fixed:"81,81"`   //(1)81
	TimeZone                string `fixed:"82,84"`   //(3)82 thru 84
	DaylightIndicator       string `fixed:"85,85"`   //(1)85
	MagneticTrueIndicator   string `fixed:"86,86"`   //(1)86
	DatumCode               string `fixed:"87,89"`   //(3)87 thru 89
	Reserved90              string `fixed:"90,93"`   //(4)90 thru 93
	AirportName             string `fixed:"94,123"`  //(30)94 thru 123
	FileRecordNo            string `fixed:"124,128"` //(5)124 thru 128
	CycleDate               string `fixed:"129,132"` //(4)129 thru 132
}
