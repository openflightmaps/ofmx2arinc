package arinc

type Runway struct {
	RecordType              string `fixed:"1,1"`   //(1)1
	CustomerAreaCode        string `fixed:"2,4"`   //(3)2 thru 4
	SectionCode             string `fixed:"5,5"`   //(1)5
	Blank6                  string `fixed:"6,6"`   //(1)6
	AirportICAOIdentifier   string `fixed:"7,10"`  //(4)7 thru 10
	ICAOCode1               string `fixed:"11,12"` //(2)11 thru 12
	SubsectionCode          string `fixed:"13,13"` //(1)13
	RunwayIdentifier        string `fixed:"14,18"` //(5)14 thru 18
	Blank19                 string `fixed:"19,21"` //(3)19 thru 21
	ContinuationRecordNo    string `fixed:"22,22"` //(1)22
	RunwayLength            string `fixed:"23,27"` //(5)23 thru 27
	RunwayMagneticBearing   string `fixed:"28,31"` //(4)28 thru 31
	Blank32                 string `fixed:"32,32"` //(1)32
	RunwayLatitude          string `fixed:"33,41"` //(9)33 thru 41
	RunwayLongitude         string `fixed:"42,51"` //(10)42 thru 51
	RunwayGradient          string `fixed:"52,56"` //(5)52 thru 56
	Blank57                 string `fixed:"57,60"` //(4)57 thru 60
	ElipsoidHeight          string `fixed:"61,66"` //(6)61 thru 66
	LandingThresholdElev    string `fixed:"67,71"` //(5)67 thru 71
	DisplacedThresholdDist  string `fixed:"72,75"` //(4)72 thru 75
	ThresholdCrossingHeight string `fixed:"76,77"` //(2)76 thru 77
	RunwayWidth             string `fixed:"78,80"` //(3)78 thru 80

	StopWay string `fixed:"87,90"` //(4)87 thru 90

	RunwayDescription string `fixed:"102,123"` //(22)102 thru 123
	FileRecordNo      string `fixed:"124,128"` //(5)124 thru 128
	CycleDate         string `fixed:"129,132"` //(4)129 thru 132
}
