package arinc

type EnrouteCommunication struct {
	RecordType             string `fixed:"1,1"`     //(1)1
	CustomerAreaCode       string `fixed:"2,4"`     //(3)2 thru 4
	SectionCode            string `fixed:"5,5"`     //(1)5
	SubsectionCode         string `fixed:"6,6"`     //(1)6
	FIRRDOIdent            string `fixed:"7,10"`    //(4)7 thru 10
	FIRUIRAddress          string `fixed:"11,14"`   //(4)11 thru 14
	Indicator              string `fixed:"15,15"`   //(1)15
	Reserved16             string `fixed:"16,18"`   //(3)16 thru 18
	RemoteName             string `fixed:"19,43"`   //(25)19 thru 43
	CommunicationsType     string `fixed:"44,46"`   //(3)44 thru 46
	CommFrequency          string `fixed:"47,53"`   //(7)47 thru 53
	GuardTransmit          string `fixed:"54,54"`   //(1)54
	FrequencyUnits         string `fixed:"55,55"`   //(1)55
	ContinuationRecordNo   string `fixed:"56,56"`   //(1)56
	ServiceIndicator       string `fixed:"57,59"`   //(3)57 thru 59
	RadarService           string `fixed:"60,60"`   //(1)60
	Modulation             string `fixed:"61,61"`   //(1)61
	SignalEmission         string `fixed:"62,62"`   //(1)62
	Latitude               string `fixed:"64,71"`   //(9)63 thru 71
	Longitude              string `fixed:"72,81"`   //(10)72 thru 81
	MagneticVariation      string `fixed:"82,86"`   //(5)82 thru 86
	FacilityElevation      string `fixed:"87,91"`   //(5)87 thru 91
	H24Indicator           string `fixed:"92,92"`   //(1)92
	AltitudeDescription    string `fixed:"93,93"`   //(1)93 thru 93
	CommunicationAltitude1 string `fixed:"94,98"`   //(5)94 thru 98
	CommunicationAltitude2 string `fixed:"99,103"`  //(1)99 thru 103
	RemoteFacility         string `fixed:"104,107"` //(4)104 thru 107
	ICAOCode               string `fixed:"108,109"` //(2)108 thru 109
	SectionCode2           string `fixed:"110,110"` //(1)110
	SubsectionCode2        string `fixed:"111,111"` //(1)111
	Reserved112            string `fixed:"112,123"` //(12)112 thru 123
	FileRecordNo           string `fixed:"124,128"` //(5)124 thru 128
	CycleDate              string `fixed:"129,132"` //(4)129 thru 132
}

type AirportCommunication struct {
	RecordType              string `fixed:"1,1"`   //(1)1
	CustomerAreaCode        string `fixed:"2,4"`   //(3)2 thru 4
	SectionCode             string `fixed:"5,5"`   //(1)5
	Blank6                  string `fixed:"6,6"`   //(1)6
	AirportICAOIdentifier   string `fixed:"7,10"`  //(4)7 thru 10
	ICAOCode1               string `fixed:"11,12"` //(2)11 thru 12
	SubsectionCode          string `fixed:"13,13"` //(1)13
	CommunicationsType      string `fixed:"14,16"` //(3)14 thru 16
	CommunicationsFrequency string `fixed:"17,23"` //(7)17 thru 23
	GuardTransmit           string `fixed:"24,24"` //(1)24
	FrequencyUnits          string `fixed:"25,25"` //(1)25
	ContinuationRecordNo    string `fixed:"26,26"` //(1)26
	ServiceIndicator        string `fixed:"27,29"` //(3)27 thru 29

	Latitude          string `fixed:"33,41"` //(9)33 thru 41
	Longitude         string `fixed:"42,51"` //(10)42 thru 51
	MagneticVariation string `fixed:"52,56"` //(5)52 thru 56
	FacilityElevation string `fixed:"57,61"` //(5)57 thru 61
	H24Indicator      string `fixed:"62,62"` //(1)62

	CallSign     string `fixed:"99,123"`  //(25)99 thru 123
	FileRecordNo string `fixed:"124,128"` //(5)124 thru 128
	CycleDate    string `fixed:"129,132"` //(4)129 thru 132
}
