package arinc

type VhfNavAid struct {
	RecordType            string `fixed:"1,1"`     //(1)1
	CustomerAreaCode      string `fixed:"2,4"`     //(3)2 thru 4
	SectionCode           string `fixed:"5,5"`     //(1)5
	SubsectionCode        string `fixed:"6,6"`     //(1)6
	AirportICAOIdentifier string `fixed:"7,10"`    //(4)7 thru 10
	ICAOCode1             string `fixed:"11,12"`   //(2)11 thru 12
	Blank13               string `fixed:"13,13"`   //(1)13
	VORIdentifier         string `fixed:"14,17"`   //(4)14 thru 17
	Blank18               string `fixed:"18,19"`   //(2)18
	ICAOCode2             string `fixed:"20,21"`   //(2)20 thru 21
	ContinuationRecordNo  string `fixed:"22,22"`   //(1)22
	VORFrequency          string `fixed:"23,27"`   //(5)23
	NAVAIDClass           string `fixed:"28,32"`   //(5)28 thru 32
	VORLatitude           string `fixed:"33,41"`   //(9)33 thru 41
	VORLongitude          string `fixed:"42,51"`   //(10)42 thru 51
	DMEIdent              string `fixed:"52,55"`   //(4)52 thru 55
	DMELatitude           string `fixed:"56,64"`   //(9)56 thru 64
	DMELongitude          string `fixed:"65,74"`   //(10)65 thru 75
	StationDeclination    string `fixed:"75,79"`   //(5)75 thru 79
	DMEElevation          string `fixed:"80,84"`   //(5)80 thru 84
	FigureOfMerit         string `fixed:"85,85"`   //(1)85 thru 85
	ILSDMEBias            string `fixed:"86,87"`   //(2)86 thru 87
	FrequencyProtection   string `fixed:"88,90"`   //(3)88 thru 90
	DatumCode             string `fixed:"91,93"`   //(3)91 thru 93
	VORName               string `fixed:"94,123"`  //(30)94 thru 123
	FileRecordNo          string `fixed:"124,128"` //(5)124 thru 128
	CycleDate             string `fixed:"129,132"` //(4)129 thru 132
}
