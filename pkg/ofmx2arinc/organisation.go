package ofmx2arinc

import (
	"errors"
	"fmt"
	"github.com/ianlopshire/go-fixedwidth"
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
	"gitlab.com/openflightmaps/ofmx2arinc/pkg/arinc"
	"math"
	"strconv"
)

func FqyToARINC(f *types.Fqy, fmap types.FeatureMap, currentRecNum *int, cycleDate string) (string, error) {
	var err error
	comm := arinc.AirportCommunication{}
	comm.RecordType = arinc.RecordTypeStandard
	comm.CustomerAreaCode = arinc.ARINC_AREA_CODE_EUR
	comm.SectionCode = arinc.SectionCodeAirport
	comm.SubsectionCode = arinc.SubSectionCodeCommunications
	if len(f.FqyUid.SerUid.UniUid.Region) > 0 {
		comm.ICAOCode1 = f.FqyUid.SerUid.UniUid.Region[0:2]
	} else {
		return "", errors.New("ERROR: missing region on comm: " + f.FqyUid.SerUid.UniUid.TxtName)
	}

	comm.ContinuationRecordNo = "0"

	if freq, err := strconv.ParseFloat(f.FqyUid.ValFreqTrans, 64); err == nil {
		if freq < 30 || freq > 200 {
			return "", errors.New("WARNING: only VHF supported, ignoring.")
		}
		comm.CommunicationsFrequency = fwidth(int(math.Round(freq*arinc.ARINC_FREQ_FACTOR_VHF)), arinc.ARINC_FREQ_WIDTH_VHF)
		comm.FrequencyUnits = arinc.ARINC_FREQ_UNIT_VHF
	} else {
		// TODO
		return "", errors.New("invalid frequency: " + f.ValFreqRec)
	}

	ser := f.FqyUid.SerUid.Ref()
	if ser == nil {
		return "", errors.New("WARNING: frequency has no SER, ignoring")
	}

	if comm.CommunicationsType, err = map_comm_type(ser.SerUid.CodeType); err != nil {
		return "", err
	}
	if comm.ServiceIndicator, err = map_comm_service(ser.SerUid.CodeType); err != nil {
		return "", err
	}

	comm.CallSign = str2arinc(f.Cdl.TxtCallSign, 25)

	uni := f.FqyUid.SerUid.UniUid.Ref()
	if uni == nil {
		return "", errors.New("WARNING: frequency has no UNI, ignoring")
	}

	if uni.AhpUid == nil {
		return "", errors.New("WARNING: Fqy: Uni has no AhpUid, ignoring")
	} else {
		ahp := uni.AhpUid.Ref()

		if ahp == nil {
			return "", errors.New("WARNING: Fqy: Uni ref to Ahp invalid, ignoring")
		}

		comm.AirportICAOIdentifier = ahp.CodeIcao

		if comm.Latitude, err = lat2arinc(ahp.GeoLat); err != nil {
			return "", err
		}
		if comm.Longitude, err = long2arinc(ahp.GeoLong); err != nil {
			return "", err
		}
		comm.MagneticVariation, err = decl2arinc(ahp.ValMagVar)

		if elev, err := convertUnit(ahp.ValElev, ahp.UomDistVer, "FT"); err == nil {
			comm.FacilityElevation = fwidths(strconv.Itoa(int(math.Round(elev))), 5)
		}
		// TODO: H24Indication
	}

	*currentRecNum++
	comm.FileRecordNo = fmt.Sprintf("%.5d", *currentRecNum)
	comm.CycleDate = cycleDate

	v, err := fixedwidth.Marshal(comm)
	return string(v) + "\n", err
}
