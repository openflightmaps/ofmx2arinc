package ofmx2arinc

import (
	"errors"
	"fmt"
	"github.com/mozillazg/go-unidecode"
	ofmx "gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
	"log"
	"math"
	"strconv"
	"strings"
)

func convertUnit(val string, fromUnit string, toUnit string) (float64, error) {
	vat := strings.Trim(val, " ")
	if vat != val {
		// TODO: drop this
		log.Printf("WARNING: invalid value, contains spaces: %s\n", val)
		val = vat
	}
	x, err := strconv.ParseFloat(val, 64)
	if err != nil {
		return 0, err
	} else if fromUnit == toUnit {
		return x, nil
	} else if fromUnit == "M" && toUnit == "FT" {
		return math.Round(x * 3.2808), nil
	} else if fromUnit == "FL" && toUnit == "FT" {
		return x * 100, nil
	} else if fromUnit == "KHZ" && toUnit == "MHZ" {
		return math.Round(x / 1000), nil
	}
	return 0, errors.New("Invalid conversion from " + fromUnit + " to " + toUnit)
}

func fwidth(v int, width int) string {
	x := strconv.Itoa(v)
	return fwidths(x, width)
}

func fwidths(x string, width int) string {
	for len(x) < width {
		x = "0" + x
	}
	return x
}

func lat2arinc(val string) (string, error) {
	var res string
	if len(val) > 0 {
		var x = val[len(val)-1 : len(val)]
		var v = val[0 : len(val)-1]
		if _, err := strconv.ParseFloat(x, 64); err == nil { // gml syntax
			if f, _ := strconv.ParseFloat(val, 64); f < 0 {
				x = "S"
			} else {
				x = "N"
			}
			// use full string
			v = val
		}
		if f, err := strconv.ParseFloat(v, 64); err == nil {
			f = math.Abs(f)
			var inms = int(math.Round(f * 60 * 60 * 100))
			var ms = inms % 100
			var s = (inms / 100) % 60
			var m = (inms / 100 / 60) % 60
			var d = inms / 100 / 60 / 60
			res = x + fwidth(d, 2) + fwidth(m, 2) + fwidth(s, 2) + fwidth(ms, 2)
		}
	} else {
		return "", errors.New("WARNING: lat2arinc empty value")
	}
	return res, nil
}

func long2arinc(val string) (string, error) {
	var res string
	if len(val) > 0 {
		var x = val[len(val)-1 : len(val)]
		var v = val[0 : len(val)-1]
		if _, err := strconv.ParseFloat(x, 64); err == nil { // gml syntax
			if f, _ := strconv.ParseFloat(val, 64); f < 0 {
				x = "W"
			} else {
				x = "E"
			}
			// use full string
			v = val
		}
		if f, err := strconv.ParseFloat(v, 64); err == nil {
			f = math.Abs(f)
			var inms = int(math.Round(f * 60 * 60 * 100))
			var ms = inms % 100
			var s = (inms / 100) % 60
			var m = (inms / 100 / 60) % 60
			var d = inms / 100 / 60 / 60
			res = x + fwidth(d, 3) + fwidth(m, 2) + fwidth(s, 2) + fwidth(ms, 2)
		}
	} else {
		return "", errors.New("WARNING: long2arinc empty value")
	}
	return res, nil
}

func str2arinc(s string, maxLen int) string {
	s = unidecode.Unidecode(s)
	if len(s) > maxLen {
		s = s[0:maxLen]
	}
	s = strings.ReplaceAll(s, "\n", " ")

	return s
}

func mapDmeChanToVHFFreq(channel string) (string, error) {
	val := 0
	if len(channel) < 2 {
		return "", errors.New("invalid dme chan: " + channel)
	}
	xy := channel[len(channel)-1]
	if xy == 'X' {
		val += 0
	} else if xy == 'Y' {
		val += 5
	} else {
		return "", errors.New("invalid dme chan: " + channel)
	}
	left := channel[0 : len(channel)-1]
	num, err := strconv.Atoi(left)
	if err != nil {
		return "", errors.New("invalid dme chan: " + channel)
	}
	if num < 17 {
		return "", errors.New("invalid dme chan (num17): " + channel)
	} else if num < 60 {
		val += 10800 + (num-17)*10
	} else if num < 70 {
		return "", errors.New("invalid dme chan (num70): " + channel)
	} else if num < 127 {
		val += 11230 + (num-70)*10
	} else {
		return "", errors.New("invalid dme chan (num+): " + channel)
	}

	return strconv.Itoa(val), nil
}

func map_comm_type(ofm string) (string, error) {
	var comMap = map[string]string{
		"ATIS":  "UNI", // AD Info frequency, UNI
		"AFIS":  "UNI", // AD Info frequency, UNI
		"OTHER": "UNI", // AD Info frequency, UNI
		"INFO":  "INF",
		"FIC":   "INF", // Flight *Information* Center
		"FIS":   "INF", // Flight *Information* Service
		"APP":   "APP",
		"TWR":   "TWR",
		"SMC":   "GND", // Surface movement control
	}
	if result, ok := comMap[ofm]; !ok {
		return "", errors.New("WARNING: Fqy: Unknown comm type: " + ofm)
	} else {
		return result, nil
	}
}

func map_comm_service(ofm string) (string, error) {
	var comMap = map[string]string{
		//"ATIS": "ATI",
		"AFIS":  "SAA", // AD Info frequency, unicom
		"INFO":  "SAA", // AD Info frequency, unicom
		"FIC":   "F  ", // Flight *Information* Center
		"FIS":   "F  ", // Flight *Information* Service
		"ATIS":  "   ",
		"APP":   "   ",
		"TWR":   "   ",
		"SMC":   "   ", // Surface movement control
		"OTHER": "   ",
	}
	if result, ok := comMap[ofm]; !ok {
		// TODO
		return "", errors.New("WARNING: Fqy: Unknown comm service: " + ofm)
	} else {
		return result, nil
	}
}

func decl2arinc(val string) (string, error) {
	// TODO, empty ok?
	if val == "" {
		return "", nil
	}
	x, err := strconv.ParseFloat(val, 64)
	if err != nil {
		return "", errors.New(fmt.Sprintf("invalid declination: '%s'\n", val))
	}
	prefix := "E"
	if x < 0 {
		prefix = "W"
	}
	return fmt.Sprintf("%s%s", prefix, fwidth(int(math.Round(x*10)), 4)), nil
}

type altitude struct {
	limit string
	unit  string
}

func formatAltitude(vs string, uom string, code string) (altitude, error) {
	// return {limit: 'value', unit: 'M'} unit M = MSL, A = AGL
	var res altitude
	v, err := strconv.Atoi(vs)
	if err != nil {
		return res, errors.New(fmt.Sprintf("WARNING: formatAltitude, not a number:%s %s for %s", vs, uom, code))
	}

	if code == "HEI" {
		if uom != "FT" && uom != "M" {
			return res, errors.New("WARNING: invalid uom: " + uom + " for " + code)
		}

		if v == 0 { // special case for GND
			return altitude{limit: "GND", unit: ""}, nil
		}
		hei, err := convertUnit(strconv.Itoa(v), uom, "FT")
		if err != nil {
			return res, err
		}
		return altitude{limit: fwidth(int(hei), 5), unit: "A"}, nil
	} else if code == "ALT" {
		if uom != "FT" && uom != "M" {
			return res, errors.New("WARNING: invalid uom: " + uom + " for " + code)
		}
		alt, err := convertUnit(strconv.Itoa(v), uom, "FT")
		if err != nil {
			return res, err
		}
		return altitude{limit: fwidth(int(alt), 5), unit: "M"}, nil
	} else if code == "STD" {
		if uom != "FL" {
			return res, errors.New("WARNING: invalid uom: " + uom + " for " + code)
		}
		if v == 0 { // special case for MSL
			return altitude{limit: "MSL", unit: ""}, nil
		} else if v >= 500 { // special case for unlimited
			return altitude{limit: "UNLTD", unit: ""}, nil
		}
		return altitude{limit: "FL" + fwidth(v, 3), unit: "M"}, nil
	} else {
		return res, errors.New("WARNING: invalid alt code: " + code)
	}
}

func ToArinc(f ofmx.Feature, fmap ofmx.FeatureMap, shapes ofmx.ShapeMap, recnum *int, cycledate string) (string, error) {
	var err error
	var a string

	switch v := f.(type) {
	case *ofmx.Vor:
		a, err = VorToARINC(v, fmap, recnum, cycledate)
	case *ofmx.Dme:
		a, err = DmeToARINC(v, fmap, recnum, cycledate)
	case *ofmx.Ndb:
		a, err = NdbToARINC(v, fmap, recnum, cycledate)
	case *ofmx.Dpn:
		a, err = DpnToARINC(v, fmap, recnum, cycledate)
	case *ofmx.Ahp:
		a, err = AhpToARINC(v, fmap, recnum, cycledate)
	case *ofmx.Fqy:
		a, err = FqyToARINC(v, fmap, recnum, cycledate)
	case *ofmx.Rdn:
		a, err = RdnToARINC(v, fmap, recnum, cycledate)
	case *ofmx.Ase:
		a, err = AseToARINC(v, fmap, shapes, recnum, cycledate)
	default:
		return "", nil
	}

	return a, err
}
