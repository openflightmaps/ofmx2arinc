package ofmx2arinc

import (
	"errors"
	"fmt"
	"github.com/ianlopshire/go-fixedwidth"
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
	"gitlab.com/openflightmaps/ofmx2arinc/pkg/arinc"
	"log"
	"strings"
)

func AseToARINC(f *types.Ase, fmap types.FeatureMap, shapes types.ShapeMap, currentRecNum *int, cycleDate string) (string, error) {

	type ArincASType struct {
		as_type       string
		is_restricted bool
		is_controlled bool
		is_firuir     bool
		res_type      string
		cas_type      string
	}
	// record types:
	// FIR/UIR (UF)
	// Controlled airspace (UC) arinc_spec.as_ctl
	// *5.213 Controlled Airspace Type (ARSP TYPE)
	// *Field
	// A - Class C Airspace (Was ARSA within the USA).
	// C - Control Area, ICAO Designation (CTA).
	// M - Terminal Control Area, ICAO Designation (TMA or TCA).
	// R - Radar Zone or Radar Area (Was TRSA within the USA).
	// T - Class B Airspace (Was TCA with the USA).
	// Z - Class D Airspace within the USA, Control Zone, ICAO Designation (CTR).

	// Restrictive airspaces (UR) arinc_spec.as_res

	var as_types = map[string]ArincASType{
		"D": {
			as_type: arinc.SubSectionCodeRestrictiveAirspace,
			is_restricted: true,
			res_type:      "D",
		},
		"P": {
			as_type: arinc.SubSectionCodeRestrictiveAirspace,
			is_restricted: true,
			res_type:      "P",
		},
		"R": {
			as_type: arinc.SubSectionCodeRestrictiveAirspace,
			is_restricted: true,
			res_type:      "R",
		},
		"GLDR": {
			// IGNORED
			is_restricted: false,
			res_type: "A",
		},
		"HPGLDR": {
			as_type: arinc.SubSectionCodeRestrictiveAirspace,
			is_restricted: true,
			res_type:      "W",
		},
		"ACRO": {
			// IGNORED
			is_restricted: false,
			res_type:      "A",
		},
		"CTR": {
			as_type: arinc.SubSectionCodeControlledAirspace,
			is_controlled: true,
			//      cas_type: "C", // TODO: for euronav test disabeld
			cas_type: "M",
		},
		"TMA": {
			as_type: arinc.SubSectionCodeControlledAirspace,
			is_controlled: true,
			cas_type:      "M",
		},
		"MTMA": {
			as_type: arinc.SubSectionCodeControlledAirspace,
			is_controlled: true,
			cas_type:      "R", // TODO:
		},
		"CTA": {
			as_type: arinc.SubSectionCodeControlledAirspace,
			is_controlled: true,
			cas_type:      "C",
		},
		"UTA": {
			// IGNORED
			is_controlled: false,
			//      cas_type: "C", // TODO: for euronav test disabeld
			cas_type: "X",
		},
		"TSA": {
			// IGNORED
			is_restricted: false,
			res_type:      "A",
		},
		"TRA": {
			as_type: arinc.SubSectionCodeRestrictiveAirspace,
			is_restricted: true,
			res_type:      "G",
		},
		"TMZ": {
			// IGNORED
			is_restricted: false,
			//      cas_type: "C", // TODO: for euronav test disabeld
			res_type: "R",
		},
		"RMZ": {
			as_type: arinc.SubSectionCodeRestrictiveAirspace,
			is_restricted: true,
			res_type:      "R",
		},
		"ATZ": {
			// IGNORED
			is_restricted: false,
			res_type:      "A",
		},
		"MATZ": {
			// IGNORED
			is_restricted: false,
			res_type:      "D",
		},
		"NRA": {
			as_type: arinc.SubSectionCodeRestrictiveAirspace,
			is_restricted: true,
			res_type:      "P",
		},
		"MTA": { // military training area
			as_type: arinc.SubSectionCodeRestrictiveAirspace,
			is_restricted: true,
			res_type:      "P",
		},
		"CBA": {
			// IGNORED
			is_restricted: false,
			res_type:      "A",
		},
		"FIR": {
			is_firuir: true,
		},
		"UIR": {
			// IGNORED
		},
		"D-OTHER": {
			// IGNORED
		},
		"RAS": {
			// IGNORED
		},
		"SECTOR": {
			// IGNORED
		},
		"DRA": { // modell flying area
			// IGNORED
		},
		"DRAFT": {
			// IGNORED
		},
		"PARA": {
			// IGNORED
		},
		"DRONE": {
			// IGNORED
		},
		"HTZ": {
			// IGNORED
		},
		"TRA_GA": {
			// IGNORED
		},
		"MODEL": {
			// IGNORED
		},
		"RC PLANES": {
			// IGNORED
		},
		"ULM": {
			// IGNORED
		},
		// non codetype airspace, e.g. Class E in Germany
		"": {
			as_type: arinc.SubSectionCodeControlledAirspace,
			is_controlled: true, // typical echo airspace
			cas_type:      "C",
		},
	}

	baseAse := f

	if f.AseUid.CodeType == "CLASS" {
		// CLASS layer
		if f.Adg != nil {
			baseAse = f.Adg.AseUidSameExtent.Ref()
			if baseAse == nil || baseAse.AseUid.Ref() == nil {
				return "", errors.New("Adg not found: "+ f.Adg.AseUidSameExtent.String())
			}
		} else {
			return "", errors.New("Adg is nil")
		}
	}

	at, ok := as_types[baseAse.AseUid.CodeType]
	if !ok {
		// TODO
		return "", errors.New("WARNING: invalid codeType:" + baseAse.AseUid.CodeType)
	}

	//var err error

	shape, ok := shapes[baseAse.AseUid.OriginalMid()]
	if !ok {
		return "", errors.New("shape extension not found: "+ baseAse.AseUid.OriginalMid() + " " + baseAse.AseUid.String())
	}
	gmlElems := strings.Split(shape.GmlPosList, " ")

	if at.is_restricted {
		as := arinc.RestrictiveAirspace{}
		as.RecordType = arinc.RecordTypeStandard
		as.CustomerAreaCode = arinc.ARINC_AREA_CODE_EUR
		as.SectionCode = arinc.SectionCodeAirspace
		as.SubsectionCode = arinc.SubSectionCodeRestrictiveAirspace
		as.ICAOCode = f.AseUid.Region[0:2]

		as.Designation = str2arinc(f.TxtName, 10)

		// restricted AS specific
		as.RestrictiveType = at.res_type

		if f.CodeClass == "" {
			// no codeclass, this Ase has class layers
			log.Println("WARNING: codeClass empty")
		}

		lower, err := formatAltitude(f.ValDistVerLower, f.UomDistVerLower, f.CodeDistVerLower)
		if err != nil {
			return "", err
		}
		upper, err := formatAltitude(f.ValDistVerUpper, f.UomDistVerUpper, f.CodeDistVerUpper)
		if err != nil {
			return "", err
		}

		// start at 10
		seqno := 10

		res := ""
		for i, gml := range gmlElems {
			if i == 0 {
				//first
				as.Level = "B" // All Altitudes
				// TODO: sample uses a continuation record
				as.ContinuationRecordNo = "0"
				as.LowerLimit = lower.limit
				as.LowerUnitIndicator = lower.unit
				as.UpperLimit = upper.limit
				as.UpperUnitIndicator = upper.unit
				as.RestrictiveAirspaceName = str2arinc(f.AseUid.CodeType+" "+f.TxtName, arinc.MaxLenAirspaceName)
				as.BoundaryVia = arinc.ARINC_BOUNDARY_VIA_GREAT_CIRCLE

			} else if i == len(gmlElems)-1 {
				// last
				as.BoundaryVia = arinc.ARINC_BOUNDARY_VIA_GREAT_CIRCLE_END
			} else {
				// clear fields for 2ff records
				as.Level = ""
				as.ContinuationRecordNo = "0"
				as.LowerLimit = ""
				as.LowerUnitIndicator = ""
				as.UpperLimit = ""
				as.UpperUnitIndicator = ""
				as.RestrictiveAirspaceName = ""
				as.BoundaryVia = arinc.ARINC_BOUNDARY_VIA_GREAT_CIRCLE
			}

			longlat := strings.Split(gml, ",")
			if len(longlat) < 2 {
				return "", errors.New("invalid gml:" + gml)
			}
			if as.Latitude, err = lat2arinc(longlat[1]); err != nil {
				return "", err
			}
			if as.Longitude, err = long2arinc(longlat[0]); err != nil {
				return "", err
			}

			as.SequenceNumber = fwidth(seqno, 4)
			seqno++
			*currentRecNum++
			as.FileRecordNo = fmt.Sprintf("%.5d", *currentRecNum)
			as.CycleDate = cycleDate

			v, err := fixedwidth.Marshal(as)
			if err != nil {
				return "", err
			}
			res += string(v) + "\n"

		}
		return res, nil
	} else if at.is_controlled {
		as := arinc.ControlledAirspace{}
		as.RecordType = arinc.RecordTypeStandard
		as.CustomerAreaCode = arinc.ARINC_AREA_CODE_EUR
		as.SectionCode = arinc.SectionCodeAirspace
		as.SubsectionCode = arinc.SubSectionCodeControlledAirspace
		as.ICAOCode = f.AseUid.Region[0:2]

		as.AirspaceCenter = str2arinc(f.AseUid.CodeId, 5)

		// restricted AS specific
		as.AirspaceType = at.cas_type

		if f.CodeClass == "" {
			// no codeclass
			log.Println("WARNING: codeClass empty")
		}

		lower, err := formatAltitude(f.ValDistVerLower, f.UomDistVerLower, f.CodeDistVerLower)
		if err != nil {
			return "", err
		}
		upper, err := formatAltitude(f.ValDistVerUpper, f.UomDistVerUpper, f.CodeDistVerUpper)
		if err != nil {
			return "", err
		}

		// start at 10
		seqno := 10

		res := ""
		for i, gml := range gmlElems {
			if i == 0 {
				//first
				as.Level = "B" // All Altitudes
				// TODO: sample uses a continuation record
				as.ContinuationRecordNo = "0"
				as.LowerLimit = lower.limit
				as.LowerUnitIndicator = lower.unit
				as.UpperLimit = upper.limit
				as.UpperUnitIndicator = upper.unit
				as.RestrictiveAirspaceName = str2arinc(f.AseUid.CodeType+" "+f.TxtName, arinc.MaxLenAirspaceName)
				as.BoundaryVia = arinc.ARINC_BOUNDARY_VIA_GREAT_CIRCLE

			} else if i == len(gmlElems)-1 {
				// last
				as.BoundaryVia = arinc.ARINC_BOUNDARY_VIA_GREAT_CIRCLE_END
			} else {
				// clear fields for 2ff records
				as.Level = ""
				as.ContinuationRecordNo = "0"
				as.LowerLimit = ""
				as.LowerUnitIndicator = ""
				as.UpperLimit = ""
				as.UpperUnitIndicator = ""
				as.RestrictiveAirspaceName = ""
				as.BoundaryVia = arinc.ARINC_BOUNDARY_VIA_GREAT_CIRCLE
			}

			longlat := strings.Split(gml, ",")
			if len(longlat) < 2 {
				return "", errors.New("invalid gml:" + gml)
			}
			if as.Latitude, err = lat2arinc(longlat[1]); err != nil {
				return "", err
			}
			if as.Longitude, err = long2arinc(longlat[0]); err != nil {
				return "", err
			}

			as.SequenceNumber = fwidth(seqno, 4)
			seqno++
			*currentRecNum++
			as.FileRecordNo = fmt.Sprintf("%.5d", *currentRecNum)
			as.CycleDate = cycleDate

			v, err := fixedwidth.Marshal(as)
			if err != nil {
				return "", err
			}
			res += string(v) + "\n"

		}
		return res, nil

	} else if at.is_firuir {
		as := arinc.FirUir{}
		as.RecordType = arinc.RecordTypeStandard
		as.CustomerAreaCode = arinc.ARINC_AREA_CODE_EUR
		as.SectionCode = arinc.SectionCodeAirspace
		as.SubsectionCode = arinc.SubSectionCodeFIR_UIR
		as.FirUirIdentifier = f.AseUid.CodeId

		upper, err := formatAltitude(f.ValDistVerUpper, f.UomDistVerUpper, f.CodeDistVerUpper)
		if err != nil {
			return "", err
		}

		as.FirUpperLimit = upper.limit
		as.UirLowerLimit = upper.limit
		as.UirUpperLimit = "UNLTD"
		as.ContinuationRecordNo = "0"

		switch f.AseUid.CodeType {
		case "FIR":
			as.FirUirIndicator = arinc.ARINC_FIR_UIR_INDICATOR_FIR
		default:
			return "", errors.New("invalid codetype: " + f.AseUid.CodeType)
		}
		// start at 10
		seqno := 10
		res := ""

		for i, gml := range gmlElems {
			as.SequenceNumber = fwidth(seqno, 4)
			seqno++
			if i == 0 {
				//first
				as.FirUirName = str2arinc(f.AseUid.CodeType+" "+f.TxtName, 25)
				as.BoundaryVia = arinc.ARINC_BOUNDARY_VIA_GREAT_CIRCLE
			} else if i == len(gmlElems)-1 {
				// last
				as.FirUirName = ""
				as.BoundaryVia = arinc.ARINC_BOUNDARY_VIA_GREAT_CIRCLE_END
			} else {
				as.FirUirName = ""
				as.BoundaryVia = arinc.ARINC_BOUNDARY_VIA_GREAT_CIRCLE
			}

			longlat := strings.Split(gml, ",")
			if len(longlat) < 2 {
				return "", errors.New("invalid gml:" + gml)
			}
			if as.Latitude, err = lat2arinc(longlat[1]); err != nil {
				return "", err
			}
			if as.Longitude, err = long2arinc(longlat[0]); err != nil {
				return "", err
			}

			*currentRecNum++
			as.FileRecordNo = fmt.Sprintf("%.5d", *currentRecNum)
			as.CycleDate = cycleDate

			v, err := fixedwidth.Marshal(as)
			if err != nil {
				return "", err
			}
			res += string(v) + "\n"
		}
		return res, nil
	} else if f.AseUid.CodeType == "CLASS" {
		return "", errors.New("WARNING: unhandled CLASS layer")
	} else {
		return "", errors.New("INFO: ignored codeType: "+ f.AseUid.CodeType)
	}
	// TODO
	return "", nil
}
