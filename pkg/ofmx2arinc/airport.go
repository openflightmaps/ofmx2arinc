package ofmx2arinc

import (
	"errors"
	"fmt"
	"github.com/ianlopshire/go-fixedwidth"
	ofmx "gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
	"gitlab.com/openflightmaps/ofmx2arinc/pkg/arinc"
	"log"
	"math"
	"strconv"
)

func AhpToARINC(f *ofmx.Ahp, fmap ofmx.FeatureMap, currentRecNum *int, cycleDate string) (string, error) {
	var err error
	airport := arinc.Airport{}
	airport.RecordType = arinc.RecordTypeStandard
	airport.CustomerAreaCode = arinc.ARINC_AREA_CODE_EUR
	airport.SectionCode = arinc.SectionCodeAirport
	airport.SubsectionCode = arinc.SubSectionCodeReferencePoints
	if len(f.AhpUid.Region) > 0 {
		airport.ICAOCode1 = f.AhpUid.Region[0:2]
	} else {
		// TODO
		return "", errors.New("ERROR: missing region on airport: " + f.AhpUid.CodeId)
		//airport.ICAOCode1 = f.AhpUid.CodeId[0:2]
	}
	if len(f.AhpUid.CodeId) > 4 {
		return "", errors.New("codeId longer than 4 chars")
	}
	airport.AirportICAOIdentifier = str2arinc(f.AhpUid.CodeId, 4)
	if airport.AirportICAOIdentifier == "" {
		return "", errors.New("invalid codeId on airport: " + f.TxtName)
	}
	airport.ContinuationRecordNo = "0"

	if airport.ARPLatitude, err = lat2arinc(f.GeoLat); err != nil {
		return "", err
	}
	if airport.ARPLongitude, err = long2arinc(f.GeoLong); err != nil {
		return "", err
	}
	airport.MagneticVariation, err = decl2arinc(f.ValMagVar)

	if elev, err := convertUnit(f.ValElev, f.UomDistVer, "FT"); err == nil {
		airport.AirportElevation = fwidths(strconv.Itoa(int(math.Round(elev))), 5)
	}
	airport.AirportName = str2arinc(f.TxtName, arinc.MaxLenDefault)

	// TODO
	airport.IFRCapability = "N"
	airport.PublicMilitaryIndicator = arinc.ARINC_PUBLIC
	for _, ahu := range f.Ahu {
		for _, l := range ahu.UsageLimitation {
			switch l.CodeUsageLimitation {
			case ofmx.CODE_USAGE_LIMITATION_PERMIT:
				for _, uc := range l.UsageCondition {
					for _, fc := range uc.FlightClass {
						if fc.CodeRule == "I" || fc.CodeRule == "VI" {
							airport.IFRCapability = "Y"
						}
						if fc.CodeMil == "MIL" {
							airport.PublicMilitaryIndicator = arinc.ARINC_MILITARY
						}
					}
				}
			}
		}
	}
	airport.MagneticTrueIndicator = arinc.ARINC_MAGNETIC
	airport.DatumCode = arinc.ARINC_WGE

	airport.CycleDate = cycleDate

	switch f.CodeType {
	case "AD":
		// Airport: OK
	case "AH":
		// Heliport: OK
	case "HP":
		fallthrough
	case "UL":
		fallthrough
	case "GLD":
		fallthrough
	case "ELS":
		// TODO
		log.Println("skipping codeType " + f.CodeType)
		return "", nil
	default:
		return "", errors.New("invalid codeType " + f.CodeType)
	}

	*currentRecNum++
	airport.FileRecordNo = fmt.Sprintf("%.5d", *currentRecNum)

	v, err := fixedwidth.Marshal(airport)
	return string(v) + "\n", err
}

func RdnToARINC(f *ofmx.Rdn, fmap ofmx.FeatureMap, currentRecNum *int, cycleDate string) (string, error) {
	var err error
	rwy := arinc.Runway{}
	rwy.RecordType = arinc.RecordTypeStandard
	rwy.CustomerAreaCode = arinc.ARINC_AREA_CODE_EUR
	rwy.SectionCode = arinc.SectionCodeAirport
	rwy.SubsectionCode = arinc.SubSectionCodeRunways

	if len(f.RdnUid.RwyUid.AhpUid.Region) >=2 {
		rwy.ICAOCode1 = f.RdnUid.RwyUid.AhpUid.Region[0:2]
	} else {
		// TODO
		return "", errors.New("ERROR: missing region on airport: " + f.RdnUid.RwyUid.AhpUid.CodeId)
		//rwy.ICAOCode1 = f.RdnUid.RwyUid.AhpUid.CodeId[0:2]
	}
	if len(f.RdnUid.RwyUid.AhpUid.CodeId) > 4 {
		return "", errors.New("codeId longer than 4 chars")
	}

	rwy.AirportICAOIdentifier = f.RdnUid.RwyUid.AhpUid.CodeId
	rwy.ContinuationRecordNo = "0"

	rwy.RunwayIdentifier = "RW" + fwidths(f.RdnUid.TxtDesig, 2)

	if rwy.RunwayLatitude, err = lat2arinc(f.GeoLat); err != nil {
		return "", err
	}
	if rwy.RunwayLongitude, err = long2arinc(f.GeoLong); err != nil {
		return "", err
	}

	r := f.RdnUid.RwyUid.Ref()
	if r == nil {
		return "", errors.New("ERROR: Rdn: Invalid Rwy reference " + f.RdnUid.TxtDesig)
	}

	if len, err := convertUnit(r.ValLen, r.UomDimRwy, "FT"); err == nil {
		rwy.RunwayLength = fwidths(strconv.Itoa(int(math.Round(len))), 5)
	} else {
		return "", errors.New("ERROR: Rdn: Unable to convert RWY length: " + f.RdnUid.TxtDesig)

	}
	if f.ValMagBrg != "" {
		rwy.RunwayMagneticBearing = fwidths(f.ValMagBrg, 4)
	} else if f.ValTrueBrg != "" {
		rwy.RunwayMagneticBearing = fwidths(f.ValTrueBrg, 4)
	}

	if elev, err := convertUnit(f.ValElevTdz, f.UomElevTdz, "FT"); err == nil {
		rwy.LandingThresholdElev = fwidths(strconv.Itoa(int(math.Round(elev))), 4)
	} else {
		// TODO, warnings
		log.Println("WARNING: Rdn: cannot convert displaced threshold distance")
	}

	rdn := f.RdnUid.Ref()
	if rdn == nil {
		return "", errors.New("ERROR: Rdn: Unable to find rdn")
	}

	for _, rdd := range rdn.Rdd {
		switch rdd.RddUid.CodeType {
		case "DTHR":
			if d, err := convertUnit(rdd.ValDist, rdd.UomDist, "FT"); err == nil {
				rwy.DisplacedThresholdDist = fwidths(strconv.Itoa(int(math.Round(d))), 5)
			} else {
				// TODO, handle warnings
				log.Println("WARNING: Rdn: cannot convert displaced threshold distance")
			}
		}
	}

	if elev, err := convertUnit(r.ValWid, r.UomDimRwy, "FT"); err == nil {
		rwy.RunwayWidth = fwidths(strconv.Itoa(int(math.Round(elev))), 3)
	} else {
		// TODO, handle warnings
		log.Println("WARNING: Rdn: cannot convert RunwayWidth")
	}

	*currentRecNum++
	rwy.FileRecordNo = fmt.Sprintf("%.5d", *currentRecNum)
	rwy.CycleDate = cycleDate

	v, err := fixedwidth.Marshal(rwy)
	return string(v) + "\n", err
}
