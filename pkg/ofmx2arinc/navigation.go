package ofmx2arinc

import (
	"errors"
	"fmt"
	"github.com/ianlopshire/go-fixedwidth"
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
	"gitlab.com/openflightmaps/ofmx2arinc/pkg/arinc"
	"log"
	"math"
	"strconv"
)

func DpnToARINC(f *types.Dpn, fmap types.FeatureMap, currentRecNum *int, cycleDate string) (string, error) {
	var err error
	wp := arinc.Waypoint{}
	wp.RecordType = arinc.RecordTypeStandard
	wp.CustomerAreaCode = arinc.ARINC_AREA_CODE_EUR
	wp.ICAOCode2 = f.DpnUid.Region[0:2]

	wp.WaypointIdentifier = str2arinc(f.DpnUid.CodeId, arinc.MaxLenWaypointIdent)
	if wp.WaypointIdentifier == "" {
		//TODO
		//wp.WaypointIdentifier = str2arinc(f.TxtName, arinc.MaxLenWaypointIdent)
		return "", errors.New("invalid codeId on waypoint: " + f.TxtName)
	}
	wp.ContinuationRecordNo = "0"
	wp.WaypointType = arinc.ARINC_WAYPOINT_TYPE_VFR
	if wp.WaypointLatitude, err = lat2arinc(f.DpnUid.GeoLat); err != nil {
		return "", err
	}
	if wp.WaypointLongitude, err = long2arinc(f.DpnUid.GeoLong); err != nil {
		return "", err
	}
	wp.WaypointNameDescription = str2arinc(f.TxtName, arinc.MaxLenDefault)
	wp.CycleDate = cycleDate
	wp.DatumCode = arinc.ARINC_WGE

	switch f.CodeType {
	case "VFR-HELI":
		fallthrough
	case "VFR-RP":
		fallthrough
	case "VFR-MRP":
		fallthrough
	case "VFR-ENR":
		if f.AhpUidAssoc != nil {
			wp.SectionCode = arinc.SectionCodeAirport
			wp.SubsectionCode = arinc.SubSectionCodeTerminalWaypoints
			wp.RegionCode = f.AhpUidAssoc.CodeId
			wp.ICAOCode1 = f.DpnUid.Region[0:2]
		} else {
			wp.SectionCode = arinc.SectionCodeEnroute
			wp.SubsectionCode = arinc.SubSectionCodeWaypoints
			wp.RegionCode = arinc.ARINC_ENROUTE
		}
	case "ICAO":
		fallthrough
	case "TOWN":
		fallthrough
	case "MOUNTAIN-PASS":
		fallthrough
	case "VFR-GLDR":
		// TODO, skipped
		// log.Println("Skipping CodeType " + f.CodeType)
		return "", nil
	default:
		return "", errors.New("invalid CodeType " + f.CodeType)
	}

	*currentRecNum++
	wp.FileRecordNo = fmt.Sprintf("%.5d", *currentRecNum)
	v, err := fixedwidth.Marshal(wp)
	return string(v) + "\n", err
}

func VorToARINC(f *types.Vor, fmap types.FeatureMap, currentRecNum *int, cycleDate string) (string, error) {
	var err error
	vor := arinc.VhfNavAid{}
	vor.RecordType = arinc.RecordTypeStandard
	vor.CustomerAreaCode = arinc.ARINC_AREA_CODE_EUR
	vor.SectionCode = arinc.SectionCodeNavaid
	vor.SubsectionCode = arinc.ARINC_SUBSECTION_CODE_VHF_NAVAID
	vor.ICAOCode2 = f.VorUid.Region[0:2]
	vor.VORIdentifier = str2arinc(f.VorUid.CodeId, arinc.MaxLenWaypointIdent)
	if vor.VORIdentifier == "" {
		return "", errors.New("invalid codeId on waypoint: " + f.TxtName)
	}
	vor.ContinuationRecordNo = "0"
	if vor.VORLatitude, err = lat2arinc(f.VorUid.GeoLat); err != nil {
		return "", err
	}
	if vor.VORLongitude, err = long2arinc(f.VorUid.GeoLong); err != nil {
		return "", err
	}
	vor.VORName = str2arinc(f.TxtName, arinc.MaxLenDefault)
	if x, err := strconv.ParseFloat(f.ValFreq, 64); err == nil {
		vor.VORFrequency = fwidth(int(math.Round(x*arinc.ARINC_FREQ_FACTOR_VOR)), 5)
	} else {
		return "", err
	}

	// TODO: verify if this is correct
	vor.StationDeclination, err = decl2arinc(f.ValMagVar)
	if err != nil {
		return "", err
	}

	// TODO: low altitude
	vor.FigureOfMerit = arinc.ARINC_MERIT_LOW
	// TODO: Frequency Protection distance
	vor.FrequencyProtection = "040"

	vor.CycleDate = cycleDate
	vor.DatumCode = arinc.ARINC_WGE

	switch f.CodeType {
	case "VOR":
		// TODO, U=Range/Power undefined, W=No voice on frequency
		vor.NAVAIDClass = "V UW "
	case "DVOR":
		// TODO, U=Range/Power undefined, W=No voice on frequency
		vor.NAVAIDClass = "V UW "
	case "INVALID":
		// TODO, mark skipped
		//log.Println("Skipping CodeType " + f.CodeType)
		return "", nil
	default:
		return "", errors.New("invalid CodeType " + f.CodeType)
	}
	*currentRecNum++
	vor.FileRecordNo = fmt.Sprintf("%.5d", *currentRecNum)
	v, err := fixedwidth.Marshal(vor)
	return string(v) + "\n", err
}

func DmeToARINC(f *types.Dme, fmap types.FeatureMap, currentRecNum *int, cycleDate string) (string, error) {
	var err error
	dme := arinc.VhfNavAid{}
	dme.RecordType = arinc.RecordTypeStandard
	dme.CustomerAreaCode = arinc.ARINC_AREA_CODE_EUR
	dme.SectionCode = arinc.SectionCodeNavaid
	dme.SubsectionCode = arinc.ARINC_SUBSECTION_CODE_VHF_NAVAID
	if len(f.DmeUid.Region) >= 2 {
		dme.ICAOCode2 = f.DmeUid.Region[0:2]
	} else {
		return "", errors.New("ERROR: Dpn missing region: " + f.TxtName)
	}
	dme.VORIdentifier = str2arinc(f.DmeUid.CodeId, arinc.MaxLenWaypointIdent)
	if dme.VORIdentifier == "" {
		return "", errors.New("invalid codeId on waypoint: " + f.TxtName)
	}
	dme.ContinuationRecordNo = "0"
	if dme.DMELatitude, err = lat2arinc(f.DmeUid.GeoLat); err != nil {
		return "", err
	}
	if dme.DMELongitude, err = long2arinc(f.DmeUid.GeoLong); err != nil {
		return "", err
	}
	if elev, err := convertUnit(f.ValElev, f.UomDistVer, "FT"); err != nil {
		dme.DMEElevation = fwidths(strconv.Itoa(int(math.Round(elev))), 5)
	}

	dme.VORName = str2arinc(f.TxtName, arinc.MaxLenDefault)

	// TODO: low altitude
	dme.FigureOfMerit = arinc.ARINC_MERIT_LOW
	// TODO: Frequency Protection distance
	dme.FrequencyProtection = "040"

	dme.CycleDate = cycleDate
	dme.DatumCode = arinc.ARINC_WGE

	dme.NAVAIDClass = " D   "
	if f.VorUid != nil {
		if v, ok := fmap[f.VorUid.Hash()]; ok {
			vor := v.(*types.Vor)
			dme.NAVAIDClass = "VDUW "

			// TODO, drop this workaround
			dme.ICAOCode2 = f.VorUid.Region[0:2]
			if dme.VORLatitude, err = lat2arinc(vor.VorUid.GeoLat); err != nil {
				return "", err
			}
			if dme.VORLongitude, err = long2arinc(vor.VorUid.GeoLong); err != nil {
				return "", err
			}
			if x, err := strconv.ParseFloat(vor.ValFreq, 64); err == nil {
				dme.VORFrequency = strconv.Itoa(int(math.Round(x * arinc.ARINC_FREQ_FACTOR_VOR)))
			} else {
				return "", err
			}
			// TODO: verify if this is correct
			dme.StationDeclination, err = decl2arinc(vor.ValMagVar)
			if err != nil {
				return "", err
			}
		} else {
			return "", errors.New("DME->VOR not found: " + f.VorUid.String())
		}
	} else {
		// DME only
		if dme.VORFrequency, err = mapDmeChanToVHFFreq(f.CodeChannel); err != nil {
			// TODO, ignore for now
			log.Println(err.Error())
		}
		dme.DMEIdent = str2arinc(f.DmeUid.CodeId, arinc.MaxLenWaypointIdent)
	}

	*currentRecNum++
	dme.FileRecordNo = fmt.Sprintf("%.5d", *currentRecNum)

	v, err := fixedwidth.Marshal(dme)
	return string(v) + "\n", err

}

func NdbToARINC(f *types.Ndb, fmap types.FeatureMap, currentRecNum *int, cycleDate string) (string, error) {
	var err error
	ndb := arinc.VhfNavAid{}
	ndb.RecordType = arinc.RecordTypeStandard
	ndb.CustomerAreaCode = arinc.ARINC_AREA_CODE_EUR
	ndb.SectionCode = arinc.SectionCodeNavaid
	ndb.SubsectionCode = arinc.ARINC_SUBSECTION_CODE_NDB_NAVAID
	if len(f.NdbUid.Region) >= 2 {
		ndb.ICAOCode2 = f.NdbUid.Region[0:2]
	} else  {
		// TODO
		//ndb.ICAOCode2 = f.NdbUid.CodeId[0:2]
		return "", errors.New("Warning: Ndb missing region: " + f.TxtName)
	}
	ndb.VORIdentifier = str2arinc(f.NdbUid.CodeId, arinc.MaxLenWaypointIdent)
	if ndb.VORIdentifier == "" {
		return "", errors.New("invalid codeId on waypoint: " + f.TxtName)
	}
	ndb.ContinuationRecordNo = "0"
	if ndb.VORLatitude, err = lat2arinc(f.NdbUid.GeoLat); err != nil {
		return "", err
	}
	if ndb.VORLongitude, err = long2arinc(f.NdbUid.GeoLong); err != nil {
		return "", err
	}
	ndb.VORName = str2arinc(f.TxtName, arinc.MaxLenDefault)
	if x, err := strconv.ParseFloat(f.ValFreq, 64); err == nil {
		ndb.VORFrequency = fwidth(int(math.Round(x*arinc.ARINC_FREQ_FACTOR_NDB)), 5)
	} else {
		return "", err
	}

	ndb.CycleDate = cycleDate
	ndb.DatumCode = arinc.ARINC_WGE

	// TODO: implement correctly
	ndb.NAVAIDClass = "H MW "

	*currentRecNum++
	ndb.FileRecordNo = fmt.Sprintf("%.5d", *currentRecNum)

	v, err := fixedwidth.Marshal(ndb)
	return string(v) + "\n", err
}
