package ofmx2arinc

import (
	"gitlab.com/openflightmaps/ofmx2arinc/pkg/arinc"
	"testing"
)

func TestDmeFreqMapping(t *testing.T) {
	var TEST_MAPPING = map[string]string{
		"17X":  "10800",
		"17Y":  "10805",
		"59X":  "11220",
		"59Y":  "11225",
		"70X":  "11230",
		"70Y":  "11235",
		"126X": "11790",
		"126Y": "11795",
	}
	for k, v := range TEST_MAPPING {
		got, err := mapDmeChanToVHFFreq(k)
		if err != nil {
			t.Errorf("Error occured for frequency: %s", err)
		}
		if got != v {
			t.Errorf("Invalid frequency %s for %s. Should be %s", got, k, v)
		}

	}
}

func TestLatLong(t *testing.T) {
	var TEST_MAPPING_LAT = map[string]string{
		"47.35361111": "N47211300",
		"46.47986111N": "N46284750",
		"-47.35361111": "S47211300",
		"46.47986111S": "S46284750",
	}
	var TEST_MAPPING_LON = map[string]string{
		"11.48": "E011284800",
		"015.68613056E": "E015411007",
		"-11.48": "W011284800",
		"015.68613056W": "W015411007",
	}

	for k, v := range TEST_MAPPING_LAT {
		got, err := lat2arinc(k)
		if err != nil {
			t.Errorf("Error occured for latitude: %s", err)
		}
		if got != v {
			t.Errorf("Invalid latitude '%s' for '%s'. Should be '%s'", got, k, v)
		}
	}
	for k, v := range TEST_MAPPING_LON {
		got, err := long2arinc(k)
		if err != nil {
			t.Errorf("Error occured for longitude: %s", err)
		}
		if got != v {
			t.Errorf("Invalid longitude %s for %s. Should be %s", got, k, v)
		}
	}
}

func TestStr2Arinc(t *testing.T) {
	var TEST_MAPPING = map[string]string{
		"MARIBOR\nTOWER": "MARIBOR TOWER",
	}

	for k, v := range TEST_MAPPING {
		got := str2arinc(k, arinc.MaxLenDefault)
		if got != v {
			t.Errorf("Invalid str2arinc %s for %s. Should be %s", got, k, v)
		}
	}

}
