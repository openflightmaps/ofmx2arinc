package main

import (
	"encoding/xml"
	"flag"
	ofmx "gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
	"gitlab.com/openflightmaps/ofmx2arinc/pkg/ofmx2arinc"
	"golang.org/x/net/html/charset"
	"io"
	"log"
	"os"
)

func AddToFmap(fmap ofmx.FeatureMap, features []ofmx.Feature) {
	for _, f := range features {
		if f.Uid().OriginalMid() != "" && f.Uid().OriginalMid() != f.Uid().Hash() {
			log.Printf("WARNING: mid mismatch: %s %s/%s\n", f.Uid().String(), f.Uid().OriginalMid(), f.Uid().Hash())
		}
		_, ok := fmap[f.Uid().Hash()]
		if ok {
			log.Printf("WARNING: duplicate feature: %s\n", f.Uid().String())
			continue
		}
		//log.Printf("Loaded: %s\n", f.Uid().String())
		fmap[f.Uid().Hash()] = f
	}
}

var ofmxFileName string
var ofmxShapeFileName string
var outputFileName string
var airac string

func init() {
	flag.StringVar(&ofmxFileName, "ofmx", "", "ofmx file name")
	flag.StringVar(&ofmxShapeFileName, "shape", "", "ofmx shape file name")
	flag.StringVar(&outputFileName, "out", "", "arinc424 output file name")
	flag.StringVar(&airac, "airac", "", "AIRAC cycle. e.g. 2004")
}

func main() {
	var snap ofmx.OfmxSnapshot
	var shapeExtensions ofmx.OfmxShapeExtensions
	shapeExtensions.Shapes = make(ofmx.ShapeMap, 0)
	fmap := make(ofmx.FeatureMap, 0)
	recnum := 0

	flag.Parse()
	if airac == "" || len(airac) != 4 || ofmxFileName == "" || ofmxShapeFileName == "" {
		flag.Usage()
		os.Exit(1)
	}

	var out io.Writer
	var err error
	if outputFileName == "" {
		out = os.Stdout
	} else if out, err = os.Create(outputFileName); err != nil {
		panic(err)
	}

	fOfmx, err := os.Open(ofmxFileName)
	if err != nil {
		panic(err)
	}

	ofmxDecoder := xml.NewDecoder(fOfmx)
	ofmxDecoder.Strict = true
	ofmxDecoder.CharsetReader = charset.NewReaderLabel

	if err = ofmxDecoder.Decode(&snap); err != nil {
		panic(err)
	}

	fShape, err := os.Open(ofmxShapeFileName)
	if err != nil {
		panic(err)
	}

	ofmxShapeDecoder := xml.NewDecoder(fShape)
	ofmxShapeDecoder.Strict = true
	ofmxShapeDecoder.CharsetReader = charset.NewReaderLabel

	if err = ofmxShapeDecoder.Decode(&shapeExtensions); err != nil {
		panic(err)
	}

	AddToFmap(fmap, snap.Features)
	ofmx.UpdateReferences(fmap)
	blacklist := make(map[string]bool, 0)

	// check for referenced VORs to ensure they are not duplicated in DME/VOR
	for _, f := range snap.Features {
		switch v := f.(type) {
		case *ofmx.Dme:
			if v.VorUid != nil {
				blacklist[v.VorUid.Hash()] = true
			}
		}
	}

	errorList := make(map[ofmx.Feature]error)

	for _, f := range snap.Features {
		// check for black-listed items
		if _, ok := blacklist[f.Uid().Hash()]; ok {
			continue
		}
		lines, err := ofmx2arinc.ToArinc(f, fmap, shapeExtensions.Shapes, &recnum, airac)
		if err != nil {
			errorList[f] = err
		} else if lines != "" {
			if _, err := out.Write([]byte(lines)); err != nil {
				panic(err)
			}
		}
	}

	log.Println("Conversion report")
	log.Printf("Number of objects: %d\n", len(snap.Features))
	log.Printf("Number of objects with errors: %d\n", len(errorList))
	log.Printf("Number of objects skipped: %d\n", len(snap.Features)-len(errorList))
	for f, err := range errorList {
		log.Println(f.Uid().String() + ": " + err.Error())
	}
}
