module gitlab.com/openflightmaps/ofmx2arinc

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/ianlopshire/go-fixedwidth v0.5.0
	github.com/mozillazg/go-unidecode v0.1.1
	gitlab.com/openflightmaps/go-ofmx v0.0.3
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e
)

//replace gitlab.com/openflightmaps/go-ofmx => ../go-ofmx
